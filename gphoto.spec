%define ver	0.6.1
%define rel	1
%define prefix	/usr

Summary: GPhoto - GNOME Digital Camera Software
Name: gphoto        
Version: %ver
Release: %rel
License: GPL
Group: X11/Utilities
Source: http://www.ping.uio.no/~oka/src/gphoto/gphoto-%{ver}.tar.xz
BuildRoot: /var/tmp/gphoto-root
URL: http://www.gnu.org/software/gphoto/gphoto.html
Docdir: %{prefix}/doc
Requires: gtk2 >= 2.24.24
Requires: libgnome >= 2.32.1
Requires: libxml2 >= 2.9.1
Requires: gnome-vfs2 >= 2.24.4
Requires: libgnomeui >= 2.24.5
Requires: libgphoto2 >= 2.5.3
Requires: totem >= 3.10.1
BuildRequires: /usr/bin/gcc gtk2-devel libgnome-devel libxml2-devel gnome-vfs2-devel libgnomeui-devel 

%description
GPhoto is a collection of free programs and libraries for
integrating digital still photography devices with GNOME.

GNOME is the GNU Network Object Model Environment.

It makes your computer easy, powerful, and easy to
configure.

%prep
%setup -q

%build
CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
make

%install
rm -rf $RPM_BUILD_ROOT
make prefix=$RPM_BUILD_ROOT%{prefix} install
%find_lang %{name}

%post
/sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

#%lang(da) %{datadir}/locale/da/LC_MESSAGES/gphoto*
#%lang(de) %{datadir}/locale/de/LC_MESSAGES/gphoto*
#%lang(fr) %{datadir}/locale/fr/LC_MESSAGES/gphoto*
#%lang(hu) %{datadir}/locale/hu/LC_MESSAGES/gphoto*
#%lang(it) %{datadir}/locale/it/LC_MESSAGES/gphoto*
#%lang(nl) %{datadir}/locale/nl/LC_MESSAGES/gphoto*
#%lang(no) %{datadir}/locale/no/LC_MESSAGES/gphoto*
#%lang(pl) %{datadir}/locale/pl/LC_MESSAGES/gphoto*
#%lang(pt_BR) %{datadir}/locale/pt_BR/LC_MESSAGES/gphoto*
#%lang(ru) %{datadir}/locale/ru/LC_MESSAGES/gphoto*
#%lang(sl) %{datadir}/locale/sl/LC_MESSAGES/gphoto*
#%lang(sv) %{datadir}/locale/sv/LC_MESSAGES/gphoto*
#%lang(uk) %{datadir}/locale/uk/LC_MESSAGES/gphoto*

%files -f %{name}.lang
%doc AUTHORS COPYING FAQ MANUAL NEWS README THANKS THEMES ChangeLog
%doc %{_mandir}/man1/gphoto.1*
%doc %{_mandir}/man1/dc21x_cam.1*
%{prefix}/bin/*
%{prefix}/lib/gphoto/*
%{prefix}/share/gphoto/doc/*
%{prefix}/share/gphoto/gallery/Default/*
%{prefix}/share/gphoto/gallery/RedNGray/*
%{prefix}/share/gphoto/gallery/CSStheme/*
%{prefix}/share/gnome/apps/Graphics/*
%{prefix}/share/gphoto/gallery/CSSjimmac/gphotobutton.gif
%{prefix}/share/gphoto/gallery/CSSjimmac/index_bottom.html
%{prefix}/share/gphoto/gallery/CSSjimmac/index_top.html
%{prefix}/share/gphoto/gallery/CSSjimmac/left.gif
%{prefix}/share/gphoto/gallery/CSSjimmac/mwcos.png
%{prefix}/share/gphoto/gallery/CSSjimmac/picture.html
%{prefix}/share/gphoto/gallery/CSSjimmac/right.gif
%{prefix}/share/gphoto/gallery/CSSjimmac/styles.css
%{prefix}/share/gphoto/gallery/CSSjimmac/thumbnail.html
%{prefix}/share/gphoto/gallery/CSSjimmac/tile.png
%{prefix}/share/gphoto/gallery/CSSjimmac/title.gif
%{prefix}/share/gphoto/gallery/CSSjimmac/top.gif
%{prefix}/share/gphoto/gallery/CSSjimmac/vh40.png
